package com.lp.order.util;


import com.lp.order.vo.ResultVO;

/**
 * @author LIPENGAK
 * @Description: 类描述
 * @date 2018-10-30 14:29
 */
public class ResultVOUtil {

    public static ResultVO success(Object object){
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(0);
        resultVO.setMsg("查询成功");
        resultVO.setData(object);
        return  resultVO;
    }

}
