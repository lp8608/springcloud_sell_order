package com.lp.order.exception;

import com.lp.order.enums.ResultEnum;

/**
 * @author LIPENGAK
 * @Description: 订单异常
 * @date 2018-10-30 15:34
 */
public class OrderException extends RuntimeException {

    private Integer code;

    public OrderException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public OrderException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());
        this.code = resultEnum.getCode();
    }
}
