package com.lp.order.service;

import com.lp.order.dto.OrderDTO;

/**
 * @author LIPENGAK
 * @Description: 类描述
 * @date 2018-10-30 15:14
 */

public interface OrderService {


    /**
     * 创建订单：
     * 2. 查询商品信息
     * 3. 计算总价
     * 4. 扣库存
     * 5. 订单入库
     * @param orderDTO
     * @return
     */
    OrderDTO create(OrderDTO orderDTO);
}
