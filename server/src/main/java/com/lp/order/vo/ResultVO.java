package com.lp.order.vo;

import lombok.Data;

/**
 * @author LIPENGAK
 * @Description: HTTP请求返回的最外层的对象
 * @date 2018-10-30 13:58
 */
@Data
public class ResultVO<T> {

    /**
     * 错误码
     */
    private Integer code;
    /**
     * 提示信息
     */
    private String msg;

    /**
     * 具体内容
     */
    private T Data;

}
