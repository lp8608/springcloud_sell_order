package com.lp.order.repository;

import com.lp.order.dataobject.OrderMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author LIPENGAK
 * @Description: 类描述
 * @date 2018-10-30 14:51
 */
@Repository
public interface OrderMasterRepository extends JpaRepository<OrderMaster,String>{


}
