package com.lp.order.repository;

import com.lp.order.dataobject.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author LIPENGAK
 * @Description: 类描述
 * @date 2018-10-30 14:52
 */
public interface OrderDetailRepository extends JpaRepository<OrderDetail,String> {
}
