package com.lp.order.message;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author LIPENGAK
 * @Description: mq消息接收
 * @date 2018-11-07 21:38
 */
@Component
@Slf4j
public class MqReceiver {

    //第一种直接从指定的队列名称中读取消息，如果没有定义队列，则报错
    //@RabbitListener(queues = "myQueue")
    //第二种自动声明队列
    //@RabbitListener(queuesToDeclare = @Queue("myQueue"))
    //第三种方式 exchange和queue绑定
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue("myQueue"),
            exchange = @Exchange("myExchange")
    ))
    public void process(String message){
        log.info("receiver myQueue msg：{}" , message);
    }

    /**
     * 数码消息接收方
     * @param message
     */
    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange("myOrder"),
            value = @Queue("computerOrder"),
            key = "computer"
    ))
    public void processComputer(String message){
        log.info("computerOrder msg：{}" , message);
    }
    /**
     * 水果消息接收方
     * @param message
     */
    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange("myOrder"),
            value = @Queue("fruitOrder"),
            key = "fruit"
    ))
    public void processFruit(String message){
        log.info("fruitOrder msg：{}" , message);
    }
}
