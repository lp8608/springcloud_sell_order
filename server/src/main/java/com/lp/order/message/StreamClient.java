package com.lp.order.message;


import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author LIPENGAK
 * @Description: mq连接源定义
 * @date 2018-11-07 22:37
 */

public interface StreamClient {

    String INPUT = "input";

    @Input(StreamClient.INPUT)
    SubscribableChannel input();

}
