package com.lp.order.controller;

import com.lp.order.dataobject.ProductInfo;
import com.lp.product.client.ProductClient;
import com.lp.product.common.DecreaseStockInput;
import com.lp.product.common.ProductInfoOutput;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;


/**
 * @author LIPENGAK
 * @Description: 类描述
 * @date 2018-10-30 16:16
 */
@RestController
@RequestMapping("/")
@Slf4j
public class ClientController {

   //@Autowired
    //private LoadBalancerClient loadBalancerClient;

    //@Autowired
    //private RestTemplate restTemplate;

    @Autowired
    private ProductClient productClient;

    @GetMapping("/getProductList")
    public String getProductList(){
        String response = "";
        //第一种远程调用方式
        //RestTemplate restTemplate = new RestTemplate();
        //response = restTemplate.getForObject("http://localhost:8080/product/list",String.class);
        //logger.info("response:{}",response);

        //第二种方式 loadBalancerClient 通过应用名称获取host，port，然后使用restTemplate
        //RestTemplate restTemplate = new RestTemplate();
        //ServiceInstance instance = loadBalancerClient.choose("EUREKA_PRODUCT");
        //String url = String.format("http://%s:%s",instance.getHost(),instance.getPort())+ "/product/list";
        //response = restTemplate.getForObject(url,String.class);

        //第三种方式通过LoadBalanced注解 ,可以再restTemplate里面使用应用的名称
        //response = restTemplate.getForObject("http://EUREKA_PRODUCT/product/list",String.class);


        List<ProductInfoOutput> productInfoList = productClient.listForOrder(Arrays.asList("157875196366160022"));
        log.info("productInfoList:{}" ,productInfoList);
        return response;
    }



    @GetMapping("/productDecreaseStock")
    public  String decreaseStock(){

        productClient.decreaseStock(Arrays.asList(new DecreaseStockInput("164103465734242707", 2)));
        return "ok";
    }
}
