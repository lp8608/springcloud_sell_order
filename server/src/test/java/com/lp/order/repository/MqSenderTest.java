package com.lp.order.repository;

import com.lp.order.OrderApplicationTests;
import org.junit.Test;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author LIPENGAK
 * @Description: 发送mq消息
 * @date 2018-11-07 21:39
 */
@Component
public class MqSenderTest extends OrderApplicationTests {

    @Autowired
    private AmqpTemplate amqpTemplate;


    @Test
    public void sender(){
        amqpTemplate.convertAndSend("myQueue", "now " + new Date());
    }

    @Test
    public void senderOrder(){
        amqpTemplate.convertAndSend("myOrder", "computer","电脑订单");
        amqpTemplate.convertAndSend("myOrder", "fruit","水果订单");
        amqpTemplate.convertAndSend("myExchange","","myQueue msg");
    }

}
